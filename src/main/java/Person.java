import java.time.LocalDate;
import java.time.Period;

public class Person {

    private final String firstName;
    private String lastName;
    private float weight;
    private float height;
    private Gender gender;
    private LocalDate birthday;

    public Person(String firstName, String lastName, float weight, float height, Gender gender, LocalDate birthday) {
        Requires.Str.NotNullOrEmpty("First name", firstName);
        this.firstName = firstName;

        setLastName(lastName);

        Requires.DateTime.NotNull(birthday,"birthday");
        Requires.DateTime.NotFuture(birthday,"birthday");
        this.birthday = birthday;


        this.weight = weight;
        this.height = height;
        this.gender = gender;

    }

    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        Requires.Str.NotNullOrEmpty(lastName, "Last name");
        this.lastName = lastName;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }


    private String getFullName() {
        return firstName + " " + lastName;
    }

    public int getAge() {
        return Period.between(birthday, LocalDate.now()).getYears();
    }

    @Override
    public String toString() {
        return "Person:\n{\t" +
                "firstName:'" + firstName + '\'' +
                ",\n\t lastName: '" + lastName + '\'' +
                ",\n\t weight: " + weight +
                ",\n\t height: " + height +
                ",\n\t gender: " + gender +
                ",\n\t birthday: " + birthday +
                "}\n";
    }
}
