import java.time.LocalDate;

public class FutureBirthdayException extends RuntimeException {
    public FutureBirthdayException(LocalDate futureDate, String fieldName) {
        super(fieldName + " should not be in future. Given date :" + futureDate);
    }
}
