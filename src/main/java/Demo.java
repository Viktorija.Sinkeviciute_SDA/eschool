import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

public class Demo {

    public static final int ADULT_AGE = 18;

    public static void main(String[] args) {

        Person viktorija = new Person(
                "Viktorija",
                "Kurauskiene",
                64.5f,
                163,
                Gender.FEMALE,
                LocalDate.parse("1984-12-17"));
        Person vytautas = new Person(
                "Vytautas",
                "Gaidys",
                69.5f,
                175,
                Gender.FEMALE,
                LocalDate.parse("1975-10-01"));
        Person vilte = new Person(
                "Vilte",
                "Kurauskiene",
                64.5f,
                163,
                Gender.FEMALE,
                LocalDate.parse("2017-01-03"));

        Person[] persons = {viktorija, vytautas, vilte};
        Stream<Person> personStream = Arrays.stream(persons);
        personStream
                .filter(person -> isAdult(person))
                .forEach(person -> System.out.println(person));


    }



    private static boolean isAdult(Person person) {
        return person.getAge() >= ADULT_AGE;
    }
}
